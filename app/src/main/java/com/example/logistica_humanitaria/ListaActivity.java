package com.example.logistica_humanitaria;
/*import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.estados.Estado;
import com.example.estados.ProyectoEstado;*/
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
/*import android.support.v7.app.AppCompatActivity;*/
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.logistica_humanitaria.database.Estados;
import com.example.logistica_humanitaria.database.EstadosDBHelper;

import java.util.ArrayList;

import java.util.ArrayList;


public class ListaActivity extends ListActivity {
    private EstadosDBHelper proyectoEstado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        proyectoEstado = new EstadosDBHelper(this);
        proyectoEstado.openDatabase();
        ArrayList<Estados> estados = proyectoEstado.allEstados();
        if (estados.isEmpty()) {
            proyectoEstado.addEstados(this.getEstadosOnce());
        }
        MyArrayAdapter adapter = new MyArrayAdapter(this,
                R.layout.activity_estados, estados);
        setListAdapter(adapter);
//NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,
                              ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            /*if(objects.get(position).isFavorite()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{*/
            lblNombre.setTextColor(Color.BLACK);
            /*}*/
            lblNombre.setText(objects.get(position).getNombre());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    proyectoEstado.openDatabase();
                    proyectoEstado.deleteEstado(objects.get(position).get_ID());
                    proyectoEstado.close();
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    Bundle oBundle = new Bundle();

                    oBundle.putSerializable("estado", objects.get(position));

                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();

                }
            });
            return view;
        }
    }

    private ArrayList<Estados> getEstadosOnce () {
        ArrayList<Estados> Estados = new ArrayList<>();
        Estados.add(new Estados(1, "Sinaloa", 1));
        Estados.add(new Estados(2, "Estado De Mexico", 1));
        Estados.add(new Estados(3, "Sonorar", 1));
        Estados.add(new Estados(4, "Nuevo Leon", 1));
        Estados.add(new Estados(5, "Jalisco", 1));
        Estados.add(new Estados(6, "Durango", 1));
        Estados.add(new Estados(7, "Baja Califronia Sur", 1));
        Estados.add(new Estados(8, "Baja California Norte", 1));
        Estados.add(new Estados(9, "Chihuahua", 1));
        Estados.add(new Estados(10, "Nayarit", 1));
        return Estados;
    }
}



