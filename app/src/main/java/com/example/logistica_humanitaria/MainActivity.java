package com.example.logistica_humanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.logistica_humanitaria.database.Estados;
import com.example.logistica_humanitaria.database.EstadosDBHelper;

public class MainActivity extends AppCompatActivity {
    private EditText edtNombre;
    private Estados savedEstado;
    private int id;
    private int estatus;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completo = true;
                if (edtNombre.getText().toString().equals("")) {
                    edtNombre.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo) {
                    EstadosDBHelper source = new EstadosDBHelper(MainActivity.this);
                    source.openDatabase();

                    Estados nEstado = new Estado();
                    nEstado.setNombre(edtNombre.getText().toString());
                    nEstado.setEstatus(1);

                    if (savedEstado == null) {
                        source.insertEstado(nEstado);

                        Toast.makeText(MainActivity.this,R.string.mensaje,

                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateEstado(nEstado,id);

                        Toast.makeText(MainActivity.this, R.string.mensajeedit,

                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    }

                    source.close();

                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListaActivity.class);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
            estatus = estado.getEstatus();
        }else{
            limpiar();
        }
    }public void limpiar(){
        savedEstado = null;
        edtNombre.setText("");
    }
}



