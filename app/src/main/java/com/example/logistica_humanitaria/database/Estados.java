package com.example.logistica_humanitaria.database;

import java.io.Serializable;

public class Estados implements Serializable {
    private int ID;
    private String nombre_estado;

    public Estados(){
        this.setID(0);
        this.setNombre_estado("");
    }
    public Estados(int ID,String nombre_estado){
        this.setID(ID);
        this.setNombre_estado(nombre_estado);

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre_estado() {
        return nombre_estado;
    }

    public void setNombre_estado(String nombre_estado) {
        this.nombre_estado = nombre_estado;
    }
}
